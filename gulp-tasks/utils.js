import gutil from 'gulp-util'

// Set global config
//export const minifyJsOnly = true

// declare folder names for source and distrabution files
export const dirs = {
    src: 'src',
    dest: 'dist',
}

// Set autoprefixer browser targets
export const browserPrexfixes = 'last 2 version'

// error function
export function handleErr(error) {
    gutil.log((gutil.colors.bold.bgRed(error.name)))
    

    // End the task
    this.emit('end');

    // Notify what's wrong
    gutil.log(gutil.colors.bold.yellow('\n'+'   On Line: '+error.loc.line+'\n'+'   In File: '+error.id))
    gutil.log(gutil.colors.bold.magenta(error.message))   
}
