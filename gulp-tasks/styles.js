/*
*   Gulp tasks for Sass and Postcss.
*/

import gulp from 'gulp'
import sass from 'gulp-sass'
import postcss from 'gulp-postcss'
import sourcemaps from 'gulp-sourcemaps'
import rename from 'gulp-rename'
import cssnano from 'gulp-cssnano'
import gutil from 'gulp-util'
import {dirs, handleErr, browserPrexfixes} from './utils.js'

// sass task
export function sassTasks() {
    return gulp.src(`${dirs.src}/sass/*.scss`)
        //.on('end', () => {gutil.log(gutil.colors.bgBlue('Converting Sass...'))})
        .on('error', handleErr)
        .pipe(sourcemaps.init())
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(`${dirs.src}/sass`));
}

// postcss task
export function postcssTasks() {
    return gulp.src(`${dirs.src}/sass/main.css`)
        .pipe(postcss([
            require('postcss-fixes'),
            require('autoprefixer')
                ({
                    browsers: [browserPrexfixes]
                })]))
        .on('error', handleErr)
        .pipe(gulp.dest(`${dirs.dest}/css/`))
        .pipe(sourcemaps.init())
        .pipe(cssnano())
        .pipe(rename({ suffix: '.min' }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(`${dirs.dest}/css/`));
}