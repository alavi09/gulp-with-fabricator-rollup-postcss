/*
*   Gulp tasks for transforming Es6 to Es5 via Babel and Rollup.
*
*/

import gulp from 'gulp'
import babel from 'rollup-plugin-babel'
import rollup from 'rollup-stream'
import source from 'vinyl-source-stream'
import buffer from 'vinyl-buffer'
import uglify from 'gulp-uglify'
import rename from 'gulp-rename'
import nodeResolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import {dirs, handleErr, minifyJsOnly} from './utils.js'
//import uglify from 'rollup-plugin-uglify'

// Main transform function
export default function jsTransform() {
    return rollup({
        entry: `${dirs.src}/js/main.js`,
        plugins: [
            // include node modules
            nodeResolve({
                main: true,
                browser: true
            }),
            
            commonjs(),
            // using rollup babel plugin, disable local bablerc settings
            babel({ presets: ['es2015-rollup'], babelrc: false }),
            // uglify using rollup plugin
            //uglify()
        ],
        sourceMap: true
    })
        .on('error', handleErr)
        .pipe(source('main.js'))         
        .pipe(gulp.dest(`${dirs.dest}/js/`));
}