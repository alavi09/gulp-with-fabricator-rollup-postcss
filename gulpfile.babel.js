/*
*   A gulp build for Es6 transpiling with Babel and Rollup, transforming        *   Handlebarsjs and sass support.
*      
*/

import gulp from 'gulp'
import assemble from 'fabricator-assemble'
import uglify from 'gulp-uglify'
import sourcemaps from 'gulp-sourcemaps'
import rename from 'gulp-rename'
import gutil from 'gulp-util'
import {create as bsCreate} from 'browser-sync'
import {dirs, browserPrexfixes, handleErr} from './gulp-tasks/utils.js'
import es6Transform from './gulp-tasks/es6.js'
import {sassTasks, postcssTasks} from './gulp-tasks/styles.js'

// browser sync method
const browserSync = bsCreate()

// browser sync
gulp.task('bs', () => {
    browserSync.init({
        server: {
            baseDir: dirs.dest
        },
        // Stop the browser from automatically opening
        open: false,
        // don't auto-reload all browsers following a Browsersync reload
        reloadOnRestart: true
    })
})

// es6 for borwsers
gulp.task('es6', () => {
    return es6Transform()
});

// minify js, best used for production
gulp.task('uglify', ['es6'], () => {
    return gulp.src(`${dirs.dest}/js/main.js`)
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write('.'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(`${dirs.dest}/js/`));
});

// sass
gulp.task('sass', () => {
    gutil.log(gutil.colors.magenta.bgWhite("Transforming sass..."))
    return sassTasks()
});

// sync order of css tasks
gulp.task('css', ['sass'], () => {
    // do postcss tasks
    postcssTasks()
});

/* Fabricator assemble only task */
gulp.task('assemble', function () {
    return assemble({})
})

// watch tasks
gulp.task('watch', () => {
    gulp.watch(`${dirs.src}/sass/*.scss`, ['css', browserSync.reload]);
    gulp.watch(`${dirs.src}/**/*.html`, ['assemble', browserSync.reload]);
    gulp.watch(`${dirs.src}/js/**/*.js`, ['es6', browserSync.reload]);
});

gulp.task('default', ['es6', 'assemble', 'css', 'bs', 'watch'], () => {
    gutil.log(gutil.colors.blue.bgWhite("HTTP Server running!"))
    gutil.log(gutil.colors.blue.bgWhite("Visit http://localhost:3000"))
});